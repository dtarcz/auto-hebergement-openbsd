Auto-hébergement facile avec OpenBSD
====================================

Ce dépôt git permet à chacun de contribuer à l'amélioration de ce document disponible sur http://yeuxdelibad.net/ah 

Afin de participer, vous pouvez : 

- Ouvrir une nouvelle *Issue*,
- Cloner le dépôt et proposer des modifications : 
    1. Forker le projet via l'interface web puis cloner votre fork localement : 
       ``git clone git@framagit.org:$VOTRENOM/auto-hebergement-openbsd.git``
    2. Créer une branche : `git checkout -b votremodif`
    3. Faire vos changements : `git add`, `git commit`. N'oubliez
       pas de vous citer comme auteur dans l'en-tête du fichier
       modifié/ajouté.
    4. Poussez les changements vers votre dépôt et créer une demande
       d'intégration (*merge request*)

    > **À noter** : il est possible d'utiliser directement l'interface web pour 
    effectuer des modifications sur un fichier, en se connectant à son compte
    et en cliquant sur le bouton *Edit* quand on visualise celui-ci.
    Cela ouvrira un éditeur, puis à la validation créera un fork du projet original 
    si pas encore fait, ainsi qu'une nouvelle branche et une *merge request* pour 
    enregistrer et discuter des modifications.

Assurez-vous de travailler sur une version à jour avant de demander
l'intégration de vos changements : 

    git remote add upstream https://framagit.org/Thuban/auto-hebergement-openbsd.git
    git fetch upstream
    git pull origin master
    git pull upstream master


Rédaction
---------
La rédaction se fait au format [txt2tags](http://txt2tags.org/markup.html).

Quelques conventions d'écriture : 
- Rester simple !
- Utiliser les variables suivantes : 
    - `&NDD` : nom de domaine `chezmoi.tld` à titre d'exemple.
    - `&IPV4` : `192.0.2.2` (`192.0.2.0/24` pour les adresses publiques
    (réseau reservé pour la documentation))
    - `&IPV6` : `2001:db8::2` et `fd19:6f4d:bea8:f63a::2`
    - `&SSLCERT` : certificate `/etc/ssl/acme/votredomaine-fullchain.com.pem`
    - `&SSLKEY` : key `/etc/ssl/acme/private/votredomaine-privkey.com.pem`
- Tester.


