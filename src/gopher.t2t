Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

==Gopher==
J'en entends déjà rire en lisant ce titre. Non, le protocole gopher n'est pas
mort. Bien que très peu utilisé, on y trouve encore quelques trésors. 
Gopher vous permettra de partager des textes et des documents tout simplement.

On dit aussi que ce protocole est très léger et permet le transfert de données
même avec de minuscules bandes passantes.

Que ce soit par utilité ou par jeu, nous allons voir comment installer un
serveur gopher sous OpenBSD.

Tout d'abord, on installe le serveur avec pkg_add : 

```
# pkg_add gophernicus
```

Ensuite, on doit éditer le fichier ``/etc/inetd.conf`` pour y mettre
la ligne suivante : 

```
gopher stream tcp nowait _gophernicus /usr/local/libexec/in.gophernicus in.gophernicus -h &NDD
```

N'oubliez-pas de remplacer ``&NDD`` par votre nom de domaine puis
activez et redémarrez ce service avec rcctl : 

```
# rcctl enable inetd
# rcctl start inetd
```

À partir de ce moment là, il ne reste plus qu'à ouvrir et rediriger le port 70
(TCP).

Mettez vos fichiers textes, vos images, vos vidéos (...) dans le dossier
``/var/gopher``, ils seront automatiquement disponibles à l'adresse
``gopher://&NDD`` .


Pour personnaliser un peu plus votre site, vous pouvez créer un fichier
``gophermap`` dans lequel vous déposez un message pour les visiteurs. Terminez
ce fichier avec un ``*`` pour qu'il y ait une liste automatiquement générée des
documents disponibles.

Par exemple : 

```
 ____  _
| __ )(_) ___ _ ____   _____ _ __  _   _  ___
|  _ \| |/ _ \ '_ \ \ / / _ \ '_ \| | | |/ _ \
| |_) | |  __/ | | \ V /  __/ | | | |_| |  __/
|____/|_|\___|_| |_|\_/ \___|_| |_|\__,_|\___|
----------------------------------------------
                                  

*
```


Note : Pour accéder à un site hébergé avec le protocole gopher, vous pouvez
utiliser l'extension Firefox [OverbiteFF http://gopher.floodgap.com/overbite/].

Un site peut alors ressembler à ça : 

[img/gopherexample.png]
