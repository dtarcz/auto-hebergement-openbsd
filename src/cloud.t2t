Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

===Un cloud avec NextCloud===
[NextCloud https://nextcloud.com/] est un service qui vous permet de
synchroniser vos documents, contacts, rendez-vous sur n'importe quelle machine
grâce à ses multiples clients.

On va commencer par créer un dossier pour Nextcloud : 

```
# mkdir /var/www/htdocs/nextcloud
```

Ensuite, sans surprise, on 
[le télécharge https://nextcloud.com/install].

```
# cd /var/www/htdocs/nextcloud
# ftp "https://download.nextcloud.com/server/releases/nextcloud-11.0.2.tar.bz2"
```

Il faut maintenant vérifier l'intégrité de l'archive. Pour cela, on va
télécharger la somme sha256 : 

```
# ftp "https://download.nextcloud.com/server/releases/nextcloud-11.0.2.tar.bz2.sha256"
```

Et on vérifie l'archive : 

```
# sha256 -C nextcloud-11*.sha256 nextcloud*.tar.bz2
(SHA256) nextcloud-11.0.2.tar.bz2: OK
```

On décompresse cette archive puis on modifie les droits pour que ces nouveaux fichiers appartiennent
au serveur web : 

```
# tar xvjf nextcloud-11*.tar.bz2
# chown -R www:daemon nextcloud
```

On peut désormais éditer le fichier ``/etc/httpd.conf`` afin d'ajouter une
section pour nextcloud : 

```
server "cloud.&NDD" {
        listen on * port 80
        block return 301 "https://$SERVER_NAME$REQUEST_URI"
}

server "cloud.&NDD" {
        listen on * tls port 443
        root "/htdocs/nextcloud/nextcloud" 
        directory index index.php
        hsts
        tls {
            &SSLCERT
            &SSLKEY
        }

        # Set max upload size to 500M (in bytes)
        connection max request body 524288000

        # First deny access to the specified files
        location "/db_structure.xml" { block }
        location "/.ht*"             { block }
        location "/README"           { block }
        location "/data*"            { block }
        location "/config*"          { block }

        location "/*.php*" {
                fastcgi socket "/run/php-fpm.sock"
        }
}
```

Avant de relancer les services, on installe les dépendances PHP dont Nextcloud
aura besoin. Notez que selon ce que vous souhaitez faire avec Nextcloud, vous
devrez peut-être installer 
[davantage de modules https://huit.re/nexclout_prerequis],
ceux ci-dessous sont le minimum
retenu pour cet exemple.

```
# pkg_add php-bz2 php-curl php-gd php-intl php-mcrypt php-zip php-ldap php-pear
```


On active ces extensions : 

```
# cd /etc/php-7.0.sample
# for i in *; do ln -sf ../php-7.0.sample/$i ../php-7.0/; done
```


**Note** : Parmi ces extensions, je vous propose l'utilisation de //redis// qui mettra en
cache votre cloud pour de meilleurs performances. Cependant, il n'est pour
l'instant disponible que pour php-5.6. Si vous utilisez php-7.0 ou supérieur,
vous pouvez passer cette étape Nous l'installons puis le
lançons.

```
# pkg_add redis pecl-redis
# rcctl enable redis
# rcctl start redis
```

Puis reprenez les deux lignes permettant d'activer les extensions PHP au-dessus.



Enfin, on recharge //httpd// et PHP avec :

```
# rcctl reload httpd
# rcctl restart php70_fpm
```


Pour terminer l'installation, ouvrez un navigateur à l'adresse de votre
cloud, par exemple ``https://cloud.&NDD/`` dans l'exemple ci-dessus.

Ne reste plus qu'à suivre les étapes :

[img/nextcloud-setup1.png]


[img/nextcloud-setup2.png]

Si vous avez des erreurs à propos de l'UTF-8 qui apparaissent, lancez les
commandes suivantes pour résoudre ce problème dû au chroot : 

```
# mkdir -p /var/www/usr/share/locale/UTF-8/
# cp /usr/share/locale/UTF-8/LC_CTYPE /var/www/usr/share/locale/UTF-8/
```

Autre remarque, afin d'augmenter la limite en taille des fichiers que vous aurez à
envoyer, vous devez modifier le fichier ``/etc/php-7.0.ini`` pour changer les
valeurs dans les variables suivantes : 

```
post_max_size = 500M
upload_max_filesize = 500M
```

Ici, nous avons utilisé la base de données SQLite. Rien ne vous empêche
d'utiliser MySQL (MariaDB) ou PostgreSQL si vous préférez. Référez-vous dans ce
cas à la
partie sur [les bases de données #bdd].


Notez que si vous souhaitez [sauvegarder #backup] votre instance de NextCloud, il suffit
de copier les fichiers présents dans (d'après
notre exemple) : 

```
/htdocs/nextcloud/nextcloud
```

N'oubliez pas de [sauvegarder #bddbackup] la base de données.
Toutefois, pensez à mettre NextCloud en mode "maintenance" le temps de la
sauvegarde. Pour cela, éditez le fichier ``config/config.php`` pour mettre
l'option ``maintenance`` à ``true`` :  

```
<?php

 "maintenance" => true,

```

Bien sûr, vous remettrez sur ``false`` une fois la sauvegarde terminée.

En cas de doute, vous pouvez lire 
[cette page https://soozx.fr/sauvegarde-locale-manuelle-planifiee-nextcloud/]
qui détaille la sauvegarde de NextCloud, ou bien entendu vous référer à la
documentation 
[officielle https://huit.re/maintenance_nextcloud].
