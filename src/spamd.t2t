



Spamd fait semblant d'être un serveur mail afin de filtrer les spams **et**
embêter les spammeurs. Il a été écrit dans l'optique d'être très efficace et ne pas ralentir la machine.
Bien sûr, il transmet les mails légitimes au serveur //smtpd// ensuite.

Ce qui est rigolo, c'est qu'il va faire en sorte de ralentir les spammeurs en communiquant tout doucement avec eux et leur faire consommer inutilement leur ressources :) .

Enfin, avantage non négligeable, il est présent par défaut dans OpenBSD.


==== Comment ça marche? ====
Deux méthodes sont possibles et compatibles. La première consiste à laisser
traîner sur le web une fausse adresse mail. Tous les messages envoyés à cette
adresse sont émis par des robots spammeurs : l'origine de ces derniers est alors
mise sur **[liste noire #blacklist]**.

L'autre méthode est le "[greylisting #greylisting]". Afin de reconnaître les
pourriels, //spamd// va mettre en attente ceux qui contactent le serveur. Ils sont mis sur **liste grise**.

Normalement, un serveur expéditeur légitime réessaie automatiquement de délivrer
le message après un certain temps. Lors du 2e essai, ce serveur est mis sur
**liste blanche** et le message est correctement distribué.

Les spammeurs ne vont pas réessayer de délivrer le message. Dans ce cas, ils sont mis après un délai assez long sur **liste noire**. Par la suite, votre serveur n'écoutera même plus les requêtes provenant de ces spammeurs.


//Attention// : Cette méthode, bien que très efficace, suppose que l'expéditeur fait les choses comme il
faut. Ce n'est malheureusement pas toujours le cas, notamment pour certains
sites marchands. Pensez-y. Vous devriez de toute manière utiliser une 
adresse poubelle comme [guerrilamail https://www.guerrillamail.com/fr/] dans
ces cas de figure. 

Afin d'enregistrer les vilains expéditeurs, il faudra exécuter la commande
spamd**b** régulièrement.

==== Mise en place ====[greylisting]

On commence par activer //spamd// au démarrage, en indiquant les options dont il aura besoin, puis on le lance : 

```
# rcctl enable spamd
# rcctl set spamd flags "-v -G 25:4:4242"
# rcctl start spamd
```

Le premier chiffre correspond au nombre de minutes qu'un expéditeur doit
attendre avant de réessayer de nous renvoyer son mail (puisque les spammeurs
envoient des salves de mails très rapidement). Le second correspond au temps qu'une entrée reste dans la liste grise, et le dernier le temps pendant lequel une entrée restera sur la liste blanche (en heures).

En complément, je vous invite à activer ``spamlogd`` qui gardera en mémoire les
expéditeurs légitimes de mail dans le temps sans repasser par la case "liste
grise". 

```
# rcctl enable spamlogd
# rcctl start spamlogd
```

On va maintenant éditer la configuration du parefeu et filtre de paquets (packet
filter). Il va envoyer à //spamd// tout le flux destiné au serveur smtp, qui
relaiera ensuite le mail normalement s'il est légitime.

Voici ce qu'il faut donc ajouter dans ``/etc/pf.conf`` : 

```
table <spamd-white> persist
table <nospamd> persist file "/etc/mail/nospamd"
pass in on egress proto tcp from any to any port smtp divert-to 127.0.0.1 port spamd
pass in on egress proto tcp from <nospamd> to any port smtp
pass in log on egress proto tcp from <spamd-white> to any port smtp
```


Dans l'ordre des lignes : 

- On crée un fichier ``/etc/mail/nospamd`` qui contiendra les IP des expéditeurs
  légitimes.
- On dévie tout ce qui devait arriver sur le port smtp pour aller dans //spamd//.
- On laisse passer toutes les IP qui étaient dans le premier fichier.
- On laisse passer les IP enregistrées par //spamd// dans la liste blanche (en
  mémoire). spamd et //pf// partagent ici la même table.


Voilà pour le parefeu. N'oubliez pas de le recharger : 

```
# pfctl -ef /etc/pf.conf
```

Il est nécessaire de régulièrement charger la liste noire des spammeurs dans
//pf//
afin que //spamd// fonctionne bien. Nous allons nous servir d'une tâche cron
pour ça. Saisissez ``# crontab -e``, puis ajoutez la ligne suivante : 


```
*/10  *  *  *  *  /usr/libexec/spamd-setup
```

Pour l'édition, référez-vous aux rappels sur l'[utilisation de vi #editfile].

Nous lançons ici ``spamd-setup`` toutes les 10 minutes. Ce temps doit être
inférieur au temps que doit attendre un expéditeur pour tenter de renvoyer son
message définit dans l'appel de //spamd//. Nous avions mis 25 minutes.



==== Piéger les spammeurs ====[blacklist]
Vous pouvez piéger les spammeurs en laissant traîner sur le web une fausse
adresse mail. Si //spamd// voit un message arriver pour cette adresse, alors il sait déjà que c'est un spam : il peut donc le mettre sur **liste noire**. Vous voilà protégés pour l'avenir.

Afin de glisser cette "adresse-piège" sur le web sans que ça soit visible par
les humains, vous pouvez l'insérer ainsi dans le code html de votre site : 

```
<a href="mailto:piege@&NDD"></a>
```

Pour indiquer à //spamd// cette adresse piège, il faut ajouter les options
suivantes à //spamdb// (attention au "b" final, ce n'est pas spamd). : 


```
# spamdb -T -a 'piege@&NDD'
```

Bien entendu, cette adresse piège ne doit pas être créée et ne risque pas de
servir à quiconque.


==== Utiliser des listes noires pré-existantes ====
Malgré ce que certains disent, les gens sympas, ça existe ! Ces derniers
ont rassemblé une liste de spammeurs qu'ils rendent publique.
Afin de les utiliser avec //spamd//, il faut éditer le fichier
``/etc/mail/spamd.conf``. À l'intérieur, nous mettrons : 

```
all:\
        :nixspam:bsdlyblack

# Nixspam recent sources list.
# Mirrored from http://www.heise.de/ix/nixspam
nixspam:\
        :black:\
        :msg="Your address %A is in the nixspam list\n\
        See http://www.heise.de/ix/nixspam/dnsbl_en/ for details":\
        :method=http:\
        :file=www.openbsd.org/spamd/nixspam.gz

bsdlyblack:\
        :black:\
        :msg="Your address %A is in the bsdly.net list":\
        :method=http:\
        :file=www.bsdly.net/~peter/bsdly.net.traplist
```

Il faut penser à ajouter après le ``all`` les deux listes utilisées par la
suite, qui sont celles de nixspam et de bsdly.net. Pour plus d'information, voir
:
: 

- [La liste nixspam http://www.heise.de/ix/nixspam]
- [La liste de Peter N. M. Hansteen http://www.bsdly.net/%7Epeter/traplist.shtml]



==== Consulter l'activité de spamd ====
Pour voir l'activité de //spamd//, lancez la commande ``spamdb`` pour voir apparaître des choses comme ça : 

```
WHITE|62.4.1.33|||1462699174|1462699174|1465809574|1|0
GREY|182.70.43.24|abts-mum-dynamic-024.43.70.182.airtelbroadband.in|<Estella32@thunderguy.co.uk>|<toto@&NDD>|1473409924|1473424324|1473424324|1|0
GREY|14.183.132.63|static.vnpt.vn|<Abby5@toddelliott.com>|<kiki@&NDD>|1473410586|1473424986|1473424986|1|0

```

On peut lire dans l'ordre : 

- Si l'IP est sur liste blanche (WHITE) ou grise (GREY).
- L'IP concernée.
- L'heure où cette entrée a été vue la première fois.
- Le moment où l'IP sera passée en liste blanche.
- Le moment où l'IP sera retirée de la base de données.
- Le nombre de fois où cette IP a été bloquée.
- Le nombre de fois où cette IP a été autorisée.


Il n'y a pas à dire, les "temps" sont illisibles pour les humains. Utiliser la commande ``date`` pour avoir un format lisible : 

```
$ date -r 1462699174
    Sun May  8 11:19:34 CEST 2016
```

Pour manuellement mettre sur **liste blanche** une IP que vous savez valide,
utilisez : 

```
# spamdb -a "62.4.1.37"
```

==== Problèmes avec le greylisting ====

Trop de personnes utilisent encore un fournisseur de mail, comme
Gmail. Ces derniers disposent de plusieurs serveurs et donc de plusieurs
adresses IP. Malheureusement, le temps que l'adresse IP du premier serveur à
tenter l'envoi d'un message soit mise
sur liste blanche, c'est un autre de leurs serveurs avec une IP différente qui
prend le relais. Au final, ils ne sont jamais mis sur liste blanche.


===== Solution communautaire =====

Vous pouvez déjà commencer par enregistrer une liste pré-remplie d'
[IP connues comme légitimes http://www.bsdly.net/%7Epeter/nospamd] 
(merci à Peter N.M. Hansteen) :

```
# ftp -o /etc/mail/nospamd http://www.bsdly.net/~peter/nospamd
```


Vous avez normalement déjà configuré le parefeu pour qu'il laisse passer les IP
contenues dans le fichier ``/etc/mail/nospamd`` dans le paragraphe sur le
[greylisting #greylisting]

Cependant, cette précaution peut ne pas toujours suffire. Je vous propose alors
de profiter de [bgp-spamd http://bgp-spamd.net]. Ce service communautaire propose de récupérer des listes de spammeurs et d'expéditeurs légitimes. Pour l'instant, seuls des serveurs vérifiés et de confiance participent à ce service, mais chacun peut en profiter. Vous serez alors tranquilles pour la plupart des cas.

Il repose sur le service bgpd qui va se charger de récupérer une autre liste
d'IP légitimes. On le configure ainsi dans le fichier ``/etc/bgpd.conf``: 

```
spamdAS="65066"

AS 65001
fib-update no    # Mandatory, to not update the 
                 # local routing table

group "spamd-bgp" {
	remote-as $spamdAS
	multihop 64
	announce none	# Do not send Route Server any information

	# us.bgp-spamd.net
	neighbor 64.142.121.62

	# eu.bgp-spamd.net
	neighbor 217.31.80.170

	# IPv6 eu.bgp-spamd.net
	neighbor 2a00:15a8:0:100:0:d91f:50aa:1
}

# 'match' is required, to remove entries when routes are withdrawn
match from group spamd-bgp community $spamdAS:42  set pftable "bgp-spamd-bypass"
```


Il y aura dans le groupe **spamd-bgp** toutes les IP connues comme
légitimes.
Elles seront ajoutées dans une table pour //pf// nommée **bgp-spamd-bypass**.

Il faut donc ajouter ces quelques lignes dans le fichier ``/etc/pf.conf`` afin
de laisser passer les expéditeurs légitimes : 

```
set limit table-entries 400000    
table <bgp-spamd-bypass> persist

#[...]

# Sous la section concernant spamd
pass in log quick on egress proto tcp from <bgp-spamd-bypass> to any port smtp
```

Référez-vous à l'exemple de [pf.conf à la fin du document #pfconf] en cas de
doute.

Relancez le parefeu : 

```
# pfctl -ef /etc/pf.conf
```

Activez et démarrez maintenant bgpd : 

```
rcctl enable bgpd
rcctl start bgpd
```


//bgpd// peut aussi récupérer des listes d'IP de **spammeurs**. On va donc créer
un script qui permettra de les mettre dans un fichier. //spamd// se chargera de
mettre sur liste noire ces IP : 

Créez le script ``/usr/local/sbin/bgp-spamd.black.sh`` pour y mettre : 


```
#!/bin/sh
AS=65066

bgpctl show rib community ${AS}:666 | 
	sed -e '1,4d' -e 's/\/.*$//' -e 's/[ \*\>]*//' > /var/mail/spamd.black

/usr/libexec/spamd-setup

/usr/bin/logger -p mail.info -t spamd-update "spamd black list updated; bgp-spamd-bypass: $(/sbin/pfctl -t bgp-spamd-bypass -T show | /usr/bin/wc -l)"

```


Ce script doit être appelé régulièrement.
Nous allons
utiliser la même technique qu'auparavant, à savoir une tâche cron. Tapez 
``# crontab -e`` puis remplacez la ligne qui appelait ``spamd-setup`` par :

```
*/10  *  *  *  *  /usr/local/sbin/bgp-spamd.black.sh 
```


```
# chmod +x /usr/local/sbin/bgp-spamd.black.sh

```

Vous noterez que ce script appelle bien ``spamd-setup``.

Nous avons presque terminé, il faut maintenant modifier le fichier de
configuration de //spamd// situé à l'emplacement ``/etc/mail/spamd.conf`` : 

```
# Configuration file for spamd.conf

all:\
	:nixspam:bgp-spamd:

# Nixspam recent sources list.
# Mirrored from http://www.heise.de/ix/nixspam
nixspam:\
	:black:\
	:msg="Your address %A is in the nixspam list\n\
	See http://www.heise.de/ix/nixspam/dnsbl_en/ for details":\
	:method=http:\
	:file=www.openbsd.org/spamd/nixspam.gz

bgp-spamd:\
	:black:\
	:msg="Your address %A has sent mail to a spamtrap\n\
	within the last 24 hours":\
	:method=file:\
	:file=/var/mail/spamd.black:
```


===== Liste blanche personnelle =====

Nous allons créer un script qui va régulièrement vérifier si un de vos contacts
n'est pas mis sur liste grise. C'est un complément simple à ce qui est fait
ci-dessus sur la base du fichier ``/etc/mail/nospamd`` qui contient déjà des IP
légitimes.

Enregistrons ce script dans ``/usr/local/bin/grey-to-white-spamd`` : 

```
#!/bin/sh
# Auteur :      thuban <thuban@yeuxdelibad.net>
# licence :     MIT

# Fill /etc/mail/allow_in_spamd with regular expression of
# lines shown by spamdb.
# Each matching line will be used to add the corresponding IP
# to /etc/mail/nospamd the spamdb whitelist

WHITE=/etc/mail/allow_in_spamd
SPAMD_WHITE=/etc/mail/nospamd

if [ ! -f $WHITE ]; then
	exit
fi

	while read line; do
		/usr/sbin/spamdb | grep "GREY|" | grep -E "$line" | cut -d'|' -f2 >> $SPAMD_WHITE
	done < $WHITE

# keep uniq
sort -u $SPAMD_WHITE -o $SPAMD_WHITE

# reload pf
/sbin/pfctl -f /etc/pf.conf
exit 0
```


Pensez à le rendre exécutable : 

```
# chmod +x /usr/local/bin/grey-to-white-spamd
```


Maintenant, remplissez le fichier ``/etc/mail/allow_in_spamd`` avec les adresses
mail que vous voulez laisser passer car vous les savez légitimes.

Notez que vous pouvez aussi mettre seulement les domaines autorisés ou un bout
de l'address mail. Par exemple : 

```
copain.org
monboss@travail.net
```


Enfin, afin que le script soit lancé régulièrement, vous pouvez : 

- Ajouter cette ligne dans le fichier ``/etc/daily.local`` :
```
/usr/local/bin/grey-to-white-spamd
```
Le script sera lancé une fois par
jour.
- Créer une nouvelle tâche cron pour lancer ce script toutes les heures. Tapez ``crontab -e`` puis ajoutez : 

```
0 * * * * /usr/local/bin/grey-to-white-spamd
```



