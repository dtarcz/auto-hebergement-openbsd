


%! encoding: utf-8

==Proxy VPN (OpenVPN)==

Un VPN veut dire "Réseau privé virtuel". Cela permet de disposer d'un tunnel
chiffré pour que tout le trafic sorte par votre serveur. Et dans ce cas, le mot
"tunnel" est une image tout à fait adaptée car un VPN présente plusieurs
intérêts, notamment : 

- Le trafic est chiffré, il ne peut donc être analysé par un tiers,
- Si vous êtes connecté sur un réseau public (hotspot, chez McDo, à l'hôtel),
  vous ignorez si le trafic n'est pas analysé. Vous souhaitez vraiment 
  aller interroger votre compte bancaire ou réaliser des achats en
  ligne alors que tout peut être intercepté ?
- Sans parler de données sensibles, il faut prendre en compte votre vie privée.
  En effet, l'administrateur réseau est en mesure de savoir que vous avez une passion pour
  le tricot d'après vos recherches et les pages web visitées, même si vous souhaitez garder ça secret.

  Un VPN permet de se protéger de ces atteintes abusives à la vie privée.
- Si vous vous connectez à partir de votre lieu de travail par exemple, il se
  peut que l'administrateur ait fermé l'accès à certains sites. Or si vous
  voulez aller sur les forums de tricot pendant votre pause, qui devrait vous en
  empêcher ?


Convaincu de l'utilité d'un VPN?

La mise en place va se passer en deux temps : 

+ Sur le serveur, nous allons générer des certificats et installer OpenVPN.
+ Sur les machines qui voudront passer par le tunnel VPN, il faudra faire passer
le trafic par notre serveur. Afin de s'identifier, des
certificats sont souvent utilisés, mais nous utiliserons plutôt des identifiants et
mots de passe.


Cette démarche n'est pas forcément facile. Prenez le temps de lire et relire ce
qui suit et suivez pas à pas les étapes proposées. 

Notez aussi que des choix ont
été faits afin de simplifier la procédure. Le résultat est donc un compromis
entre sécurité et facilité d'utilisation pour l'administrateur et l'utilisateur.
On pourra par exemple remarquer que : 

- OpenVPN ne sera pas exécuté dans un chroot. (rappel : "chroot" signifie
  "change root" pour "changer la racine /")
- On n'utilisera pas de certificat pour chaque client car c'est un processus trop pénible à gérer pour un débutant.
- On utilisera à la place une identification par mot de passe. On rappelle
  l'[importance de bien les choisir #mdp].
- Chaque utilisateur autorisé à utiliser le VPN sera créé pour l'occasion et
  sera placé dans un groupe spécifique.


Les plus aguerris voudront certainement lire la page suivante pour 
[aller plus loin http://openbsdsupport.org/openvpn-on-openbsd.html].

=== Préparation de la configuration ===
On commence par installer OpenVPN : 

```
# pkg_add openvpn openvpn_bsdauth
```

Vous voudrez peut-être lire la documentation fournie avec
le paquet : 

```
less /usr/local/share/doc/pkg-readmes/openvpn-2.3.11
```

Pour l'instant, nous allons créer les dossiers nécessaires à openvpn. Ces
derniers contiendront la configuration, les certificats, et on termine par le
dossier qui contiendra les journaux d'openvpn : 

```
# install -m 700 -d /etc/openvpn/private
# install -m 755 -d /etc/openvpn/certs
# install -m 755 -d /var/log/openvpn
```

La commande ``install`` est très pratique pour créer des dossiers avec les
permissions adéquates.

C'est tout pour l'instant, on y va doucement mais sûrement.


===Création des certificats===

On va maintenant créer les certificats et autres fichiers servant à
l'authentification du serveur. Par simplicité, nous allons utiliser l'outil
``easy-rsa`` : 

```
# pkg_add easy-rsa
```

Afin de créer les fichiers dont on aura besoin, on se place dans un dossier
temporaire ``/tmp/openvpn`` dans lequel on copie les éléments nécessaires à
easy-rsa : 

```
# mkdir -p /tmp/openvpn
# cd /tmp/openvpn
# cp -r /usr/local/share/easy-rsa/* .
```

On va maintenant configurer easy-rsa en éditant un fichier dont le petit nom est ``vars``. On copie
tout d'abord l'exemple donné : 

```
# cp vars.example vars
```

Éditez ce fichier. Vous devriez changer au moins les variables suivantes selon
votre cas: 

```
set_var EASYRSA_REQ_COUNTRY     "FR"
set_var EASYRSA_REQ_PROVINCE    "France"
set_var EASYRSA_REQ_CITY        "Nantes"
set_var EASYRSA_REQ_ORG         "PuffyCorp"
set_var EASYRSA_REQ_EMAIL       "me@&NDD"
set_var EASYRSA_REQ_OU          "PuffyCorpUnit"

set_var EASYRSA_KEY_SIZE        4096
```

Ensuite, on va générer les certificats en lançant les commandes suivantes : 

```
# ./easyrsa init-pki
# ./easyrsa build-ca nopass
# ./easyrsa build-server-full server nopass
# ./easyrsa gen-dh
```


C'est long, il y a des ``.``, des ``+`` qui apparaissent, c'est normal.

On en profite pour générer au passage le certificat de révocation, qui vous sera
peut-être utile un jour : 

```
# ./easyrsa gen-crl
```


Une fois terminé, on va changer les permissions sur les fichiers générés : 

```
chown -R _openvpn:wheel pki/*
chmod -R 600 pki/*
```

On retourne dans le dossier d'openvpn, dans lequel on copie les fichiers créés
précédemment : 

```
# cd /etc/openvpn
# cp -p /tmp/openvpn/pki/ca.crt certs/ca.crt
# cp -p /tmp/openvpn/pki/issued/server.crt certs/server.crt
# cp -p /tmp/openvpn/pki/private/server.key private/server.key
# cp -p /tmp/openvpn/pki/dh.pem dh.pem
# cp -p /tmp/openvpn/pki/crl.pem crl.pem
```

Pensez à supprimer le dossier ``/tmp/openvpn`` : 

```
# rm -rf /tmp/openvpn
```

=== Configuration d'OpenVPN ===

Afin de configurer OpenVPN, on va définir un mot de passe pour restreindre
l'administration. Mettez ce [bon mot de passe #mdp] dans le fichier
``/etc/openvpn/private/mgmt.pwd``. Modifier les permissions vers ce fichier pour
en restreindre l'accès : 

```
# chown root:wheel /etc/openvpn/private/mgmt.pwd
# chmod 600 /etc/openvpn/private/mgmt.pwd
```

Nous pouvons maintenant configurer openvpn en créant le fichier
``/etc/openvpn/server.conf``. Afin qu'il soit pré-rempli, on va copier l'exemple
fourni avec la commande : 

```
# cp /usr/local/share/examples/openvpn/sample-config-files/server.conf /etc/openvpn/server.conf
```

Éditez maintenant ce fichier. Vous devrez au moins changer ces lignes : 

```
# configuration reseau
dev tun0
server 10.8.0.0 255.255.255.0
push "redirect-gateway def1"
# On propose le resolveur de FDN
# https://www.fdn.fr/actions/dns/
push "dhcp-option DNS 80.67.169.12"
# Si le serveur a son propre resolveur
push "dhcp-option DNS 10.8.0.1"

# les certificats
ca /etc/openvpn/certs/ca.crt
cert /etc/openvpn/certs/server.crt
key /etc/openvpn/private/server.key
dh /etc/openvpn/dh.pem
crl-verify /etc/openvpn/crl.pem

# options diverses
comp-lzo
daemon openvpn
float
group _openvpn
user _openvpn
ifconfig-pool-persist /var/openvpn/ipp.txt
keepalive 10 120
management 127.0.0.1 1195 /etc/openvpn/private/mgmt.pwd
max-clients 100
persist-key
persist-tun
port 1194
proto udp

# authentification
client-cert-not-required
username-as-common-name
script-security 3 system
auth-user-pass-verify /usr/local/libexec/openvpn_bsdauth via-env
auth-nocache

# journaux
log-append  /var/log/openvpn/openvpn.log
status /var/log/openvpn/openvpn-status.log
verb 3
```


On modifie les permissions vers ce fichier : 

```
# chown root:_openvpn /etc/openvpn/server.conf
# chmod 640 /etc/openvpn/server.conf
```

Ça sera tout pour la configuration du serveur.

===Configuration réseau pour OpenVPN===

Nous devons modifier la configuration du parefeu afin de rediriger le trafic
provenant du VPN. Dans le fichier ``/etc/pf.conf``, vous aurez alors cette ligne
en plus : 

```
pass out on $ext_if from 10.8.0.0/24 to any nat-to ($ext_if)
```

Il faut aussi autoriser le trafic à travers le tunnel VPN : 

```
pass in quick on $tun_if keep state
```

N'oubliez pas au passage d'ouvrir et de rediriger le port 1194 en UDP.

On peut maintenant relancer le pare-feu avec ``pfctl -d && pfctl -ef /etc/pf.conf``.

Il faut autoriser le transfert d'IP (ou "l'IP packet forwarding") en éditant le fichier
``/etc/sysctl.conf`` pour y mettre : 

```
net.inet.ip.forwarding=1
```

Pour ne pas avoir à redémarrer le serveur, vous pouvez activer cette option avec
la commande : 

```
# sysctl net.inet.ip.forwarding=1
```

Si vous avez la chance de disposer d'une IPv6, n'oubliez pas l'équivalent : 

```
# sysctl net.inet6.ip6.forwarding=1
# echo 'net.inet6.ip6.forwarding=1' >> /etc/sysctl.conf
```





===Création des utilisateurs du VPN===
C'est presque fini! Courage!

Seuls quelques utilisateurs auront le droit d'utiliser le VPN. Les élus
autorisés seront ceux appartenant au groupe ``_openvpnusers``. 
Pour [créer ces utilisateurs #adduser]
en les mettant dans le bon groupe directement, utilisez la commande suivante : 

```
# useradd -G _openvpnusers utilisateur
```

Profitez-en pour attribuer un [mot de passe robuste #mdp] pour ce dernier : 

```
# passwd utilisateur
```

===Démarrage du VPN===

Afin de lancer le VPN automatiquement à chaque fois que le serveur
démarre, une interface de type "tunnel" doit être créée. Tout ceci peut être
réalisé automatiquement par OpenBSD en ajoutant un fichier
``/etc/hostname.tun0`` pour mettre dedans : 

```
up
!/usr/local/sbin/openvpn --config /etc/openvpn/server.conf
```

Afin de le lancer manuellement, tapez : 

```
# sh /etc/netstart
```

Vous pouvez alors vérifier que openvpn est bien en cours de fonctionnement en
regardant les journaux : 

```
# tail -f /var/log/openvpn/*
```

Afin d'arrêter openvpn pour une raison ou une autre, vous pouvez tuer son
processus avec la commande ``pkill -9 openvpn``.

=== Configuration client ===
Il existe des outils graphiques permettant de faciliter la configuration d'une connexion avec
openvpn. Libre à vous d'en installer un sur votre ordinateur ou smartphone.

Le plus souvent, vous devrez fournir le certificat du serveur afin que le client
puisse se connecter. Envoyez-le par mail ou tout autre moyen. Il s'agit du
fichier ``/etc/openvpn/certs/ca.crt``. Vous pouvez afficher son contenu avec la
commande ``cat`` : 

```
# cat /etc/openvpn/certs/ca.crt

-----BEGIN CERTIFICATE-----

MIIGNTCCAh2gAwIBAgIJALxL5Rt5/p1TMA0GCSqGSIb3D
BAMMC3lldXhkZWxpYmFkMB4XDTE2MTAyMzA5MTk0NloXD
FjE9MBIGA1UEAwwLeWV1eGRlbGliYWQwggEiMA0GCSqGS
0NRT4WgYexgM6Dh+S/tMq8JciQMUEgMwLZZrFZn7mDBt1
...
...
jqudepBh87dbZUBSbmr9QuOV1sdPA6TUp+yn8Ytyfd0M2
nyPoapWcItouatMe42gzo0f3NPkHSIhi/dXv9BGNa1jxG
OTIdoOdFmQw3BlD+TCyYDMgKpWKaFi6t9Jyji7WJst68+
u2WyqiFXS/FrWqxoTs4WWGuAM/LPzMvhhNZ5eiP/l0aIR

-----END CERTIFICATE-----
```

Ensuite, reste à copier ce contenu dans un fichier pour l'envoyer au client.

Avec la configuration proposée ci-dessus, l'utilisateur devra se connecter avec
les identifiants créés rien que pour lui.

Selon le logiciel vous permettant de vous connecter à votre VPN, la configuration générée ressemblera à ceci
pour les clients : 

```
client
dev tun0
proto udp
remote &NDD 1194
ca ca.crt
auth-user-pass
comp-lzo
auth-nocache
```

Remarquez qu'on retrouve les mêmes options que celles définies pour le serveur.
Le fichier ``ca.crt`` correspond au certificat du serveur récupéré plus tôt, qu'il
faudra enregistrer sur le client.

Une fois vos identifiants entrés, vos communications passent par votre VPN. Vous
pouvez le vérifier en utilisant un service web qui vous donne votre adresse 
[IP #IP]. Ce dernier doit afficher l'IP du
serveur et non l'IP de votre ordinateur.
