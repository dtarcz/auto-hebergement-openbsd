Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

===CardDAV et CalDAV avec Baïkal===
[Baïkal http://sabre.io/baikal/] est un serveur Cal et CardDAV permettant
de synchroniser votre calendrier et vos contacts. Il ne fait que ça,
c'est pourquoi il le fait bien tout en restant léger.

Vous aurez besoin pour l'utiliser de [PHP #php] et de [SQLite #sqlite].


Vérifiez quelle est 
[la dernière version https://github.com/fruux/Baikal/releases]
de Baikal. Vous pouvez ensuite la télécharger avec ``ftp``: 

```
# ftp -o /tmp/baikal.zip "https://github.com/fruux/Baikal/releases/download/0.4.6/baikal-0.4.6.zip"
```


On se déplace dans le dossier web pour décompresser baikal et modifier les
droits sur les fichiers : 

```
# cd /var/www/htdocs/
# unzip /tmp/baikal.zip
# chown -R www:daemon baikal
```

Ajoutez une nouvelle section dans le fichier ``/etc/httpd.conf`` pour configurer
//httpd//. Notez qu'on ne configure ici qu'un accès via une adresse en "https" : 


```

server "dav.&NDD" {
    listen on * tls port 443
    root "/htdocs/baikal/html" 
    directory index index.php
    hsts
    tls {
        &SSLCERT
        &SSLKEY
    }

    location "/.well-known/caldav" { 
        block return 301 "https://$SERVER_NAME/dav.php" 
    }
    location "/.well-known/carddav" { 
        block return 301 "https://$SERVER_NAME/dav.php" 
    }

    location "/.ht*" { block }
    location "/Core*" { block }
    location "/Specific*" { block }

    location "*.php*" {
        fastcgi socket "/run/php-fpm.sock"
    }
}
```


Reste à recharger //httpd// avec ``rcctl reload httpd``. Vous pouvez désormais vous
rendre à l'adresse ``https://dav.&NDD`` pour terminer l'installation.

[img/baikal1.png]

[img/baikal2.png]

[img/baikal3.png]


====Configuration de Thunderbird pour Baïkal====
Pour utiliser votre calendrier, vous pouvez récupérer l'excellente extension
[lightning https://support.mozilla.org/t5/Agenda/Comment-installer-Lightning-dans-Thunderbird/ta-p/17634]
pour Thunderbird.

Pour la télécharger, [c'est par ici https://addons.mozilla.org/en-US/thunderbird/addon/lightning/].
Enregistrez le fichier ``.xpi`` puis ouvrez-le dans Thunderbid à partir du menu
des modules accessible dans le menu de
Thunderbird remarquable par ses 3 traits horizontaux en haut à droite.

[img/lightning-install1.png]


Il faudra cliquer sur le petit engrenage pour choisir d'installer à la main
l'extension précédemment téléchargée : 

[img/lightning-install2.png]


Ci-dessous, vous pourrez lire des instructions issues de 
[guillaume-leduc.fr https://www.guillaume-leduc.fr/synchronisez-baikal-avec-thunderbird-et-lightning.html]
pour utiliser votre instance de Baïkal avec Thunderbird.

Vous pouvez créer un nouvel agenda en faisant un clic-droit dans la zone où sont
listés tous les calendriers.

[img/lightning4.png]

Lors de la configuration de votre agenda **CalDAV**, vous devrez renseigner l'adresse
suivante pour un agenda "sur le réseau" : 

```
https://dav.&NDD/cal.php/calendars/UTILISATEUR/ID_AGENDA/
```

Vous remplacerez ``UTILISATEUR`` et ``ID_AGENDA`` selon l'adresse visible dans
votre navigateur. **N'oubliez pas** le ``/`` final. Dans l'exemple ci-dessous,
ça donne une URL qui se termine par ``/toto/default/`` : 

[img/baikal-url.png]



Pour un carnet d'adresses, l'URL à renseigner est la suivante : 
```
https://dav.&NDD/card.php/addressbooks/UTILISATEUR/CARNET/
```

====Configuration de Rainloop pour Baïkal====
Si vous utilisez le webmail [RainLoop #rainloop], sachez qu'il est possible
d'utiliser la synchronisation des contacts avec votre instance de Baïkal.

Dans l'interface d'administration de Rainloop, allez dans l'onglet "Contacts"
puis cochez "Enable contacts" et "Allow contact sync" : 

[img/rainloop-contact.png]


Ensuite, connectez-vous en simple utilisateur. Dans les paramètres de ce dernier
accessibles en haut à droite, vous trouverez un onglet "Contact" permettant de
préciser l'adresse de votre instance Baïkal sur le même modèle que pour
Thunderbird, autrement dit : 

```
https://dav.&NDD/card.php/addressbooks/UTILISATEUR/CARNET/
```


[img/rainloop-contact2.png]

[img/rainloop-contact3.png]
