Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4



==SFTP : Envoyer des fichiers sur le serveur==[sftp]

Le protocole SFTP ressemble beaucoup au célèbre FTP qui sert surtout à
transférer des fichiers, mais se base sur un tunnel
SSH. À vrai dire, chaque utilisateur disposant d'un accès SSH peut envoyer et
télécharger des données en SFTP avec ses codes d'identification habituels.

Pour copier des fichiers, vous pouvez utiliser la commande ``scp`` qui s'utilise
ainsi : 

```
$ scp -p <port ssh> utilisateur@&NDD:/emplacement/de/destination fichier-a-copier
```

On peut aussi plus simplement utiliser un client graphique comme décrit plus bas. 

Si vous voulez déployer un serveur SFTP pour un plus grand nombre
d'utilisateurs, la mise en place d'un "chroot" est conseillée. Cette procédure
est [décrite plus loin #sftpchroot] dans le document car un peu plus compliquée.



===Ajouter un compte SFTP===

Ajouter un compte SFTP revient à créer un nouvel
utilisateur et mettre ce dernier dans le groupe ``sftpusers``.

Il faut quand même faire attention à plusieurs points : 

- L'utilisateur ne doit pouvoir faire **que** du SFTP. On va donc
lui attribuer le shell "nologin" qui ne permet pas de lancer des
commandes.
- L'utilisateur n'a pas besoin de dossier personnel dans
        ``/home``, mais aura besoin de son dossier dans
        ``/mnt/sauvegarde/sftp``.
- Il faut s'assurer que seul l'utilisateur a les droits
        d'écriture dans son dossier. On lui créera un dossier ``documents``
        prévu à cet effet.


Voici la marche à suivre : 

- Création d'un groupe sftpusers : ``groupadd sftpusers``.
- Ajout d'un utilisateur avec création du dossier personnel à l'endroit
  souhaité et ajout dans le groupe sftpusers. 
```
# useradd -G sftpusers -b /documents -s /sbin/nologin utilisateur
```

Il y a un avertissement nous indiquant que l'utilisateur n'a pas de ``/home``.
C'est justement ce que nous souhaitons.

- On modifie le mot de passe avec ``passwd utilisateur`` .
- On crée le dossier pour l'utilisateur : 
```
# mkdir -p /mnt/sauvegarde/sftp/utilisateur
```
- On crée le dossier dans lequel l'utilisateur aura le droit d'écrire : 
```
# mkdir -p /mnt/sauvegarde/sftp/utilisateur/documents
# chown utilisateur:sftpusers /mnt/sauvegarde/sftp/utilisateur/documents
```


L'utilisateur peut maintenant utiliser le protocole SFTP. Il sera enfermé dans
son dossier précédemment créé.


===Transferts avec Firefox===
C'est la solution sans doute la plus simple pour ceux qui utilisent déjà ce
navigateur. On va ajouter un module à Firefox qui
s'appelle **[Fireftp https://addons.mozilla.org/fr/firefox/addon/fireftp/]**.  

Après installation de l'addon, entrez dans la barre d'adresse de Firefox ceci : 

```
sftp://utilisateur@&NDD:222
```

Remplacez ``222`` par le port ssh du serveur (22 par défaut).
Bien sûr, "utilisateur" est aussi à remplacer 
(par exemple **jean_eudes** ou **toto**).

Un mot de passe vous est demandé, il suffit de le taper.

[img/fireftp.png]


Reste à utiliser les flèches centrales pour envoyer ou récupérer des documents.

===Transferts avec Filezilla===[filezilla]
On peut aussi utiliser le logiciel Filezilla qui permettra de se connecter au
serveur.

Vous pouvez le 
[télécharger http://filezilla-project.org/download.php?type=client]
ou l'installer directement
via le gestionnaire de paquets de votre distribution. Vérifiez qu'il s'agit bien
d'une //version à jour//.

Ouvrez Filezilla, puis cliquez sur la petite icône en haut à gauche
"Gestionnaire de sites". Une nouvelle fenêtre s'ouvre :

[img/filezilla.png]

Cliquez sur "Nouveau Site", puis remplissez les champs ainsi :
- Hôte : &NDD
- Port : 222 (à remplacer par le port SSH)
- Protocole : SFTP - SSH File Transfert Protocol
- Type d'authentification : Normale
- Identifiant : votre identifiant
- Mot de passe : votre mot de passe


Il ne vous reste plus qu'à cliquer sur Connexion.
Sélectionnez les fichiers que vous souhaitez envoyer ou récupérer, puis faites un clic-droit pour les télécharger. La
même chose est possible avec des glisser/déposer.
