Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


====Spip====

[Spip http://www.spip.net/fr] est un gestionnaire de contenu simplifié, qui
permet notamment de déployer facilement son site web.

Spip nécessite l'installation de [PHP #php] avec l'activation des modules.

=====Choix d'une base de données=====
Selon ce que vous préférez, vous pouvez choisir votre base de donnée :
[MySQL #mysql] ou [SQLite #sqlite]. MySQL est complet et conviendra pour un site
conséquent, mais est plus difficile à mettre en place. 
SQLite est extrêmement simple, tout en restant performant. Pour de
l'auto-hébergement, SQLite me semble le choix le plus approprié.

Avec SQLite, installez le paquet sqlite3 : ``pkg_add sqlite3``.
Et c'est tout! Vous pouvez passer à la partie suivante.

Pour MySQL, suivez les indications détaillées dans 
[la partie qui lui est dédié #mysql] afin de créer une nouvelle base. Pensez à
installer le paquet ``php-mysqli-7.0.*``.

=====Préparation de Spip=====
On va créer le dossier qui recevra spip : 

```
mkdir -p /var/www/htdocs/spip
```

On télécharge l'installateur de spip dans ce dossier avec la commande suivante : 

```
ftp -o /var/www/htdocs/spip/spip_loader.php "http://www.spip.net/spip-dev/INSTALL/spip_loader.php"
```

On modifie les droits pour le serveur web : 

```
chown -R www:daemon /var/www/htdocs/spip
```


=====Configuration du serveur web (http) pour spip=====
On édite le ficher ``/etc/httpd.conf`` pour y indiquer ceci : 

```
server "&NDD" {
        listen on * port 80
        block return 301 "https://$SERVER_NAME$REQUEST_URI"
        log access "owncloud.log"
}

server "&NDD" {
        listen on * tls port 443
        root "/htdocs/spip"
        directory index index.php
        hsts
        tls {
            &SSLCERT
            &SSLKEY
        }
        log access "spip.log"
        log error "spip-error.log"

        # Set max upload size to 513M (in bytes)
        connection max request body 537919488

        # Interdiction d'accès
        location "/tmp*" { block }
        location "/config*"             { block }
        location "/local*"           { block }

        # Any other PHP file
        location "/*.php*" {
                fastcgi socket "/run/php-fpm.sock"
        }
}
```



Remplacez ``&NDD`` par votre nom de domaine, et hop, le tour
est joué!
Relancez //httpd// avec cette commande : ``rcctl reload httpd``.

Dirigez-vous maintenant à l'adresse ``https://&NDD/spip_loader.php``
pour terminer l'installation.

[img/spip-install.png]

[img/spip-install2.png]

[img/spip-install3.png]

[img/spip-install4.png]

[img/spip-install5.png]

[img/spip-install6.png]

[img/spip-install7.png]

Une fois l'installation terminée, supprimez le fichier ``spip_loader.php`` : 
```
rm /var/www/htdocs/spip/spip_loader.php
```



