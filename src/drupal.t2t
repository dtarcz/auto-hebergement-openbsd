Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


====Drupal====
[Drupal https://www.drupal.org] est un gestionnaire de contenu très répandu mais
aussi relativement lourd. Cependant, vous pourrez l'utiliser pour créer toutes
sortes de sites.

Avant toutes choses, vous devez avoir déjà installé et configuré [PHP #phppp].
Vous pouvez utiliser n'importe quelle basse de données entre [SQLite #sqlite], 
[MySQL #mysql] ou [PostgreSQL #postgresql]. SQLite est le plus intéressant en
auto-hébergement et le plus simple.

On télécharge Drupal en utilisant le lien que vous pouvez retrouver à 
[cette page https://www.drupal.org/download].

```
# cd /tmp
# ftp https://ftp.drupal.org/files/projects/drupal-8.2.6.tar.gz
```


Une fois l'archive récupérée, on extrait les fichiers puis on les place dans un
dossier accessible par le serveur //httpd// qui sera ici
``/var/www/htdocs/drupal`` : 

```
# tar xvzf drupal*.tar.gz
# mv drupal-*/ /var/www/htdocs/drupal
```


N'oubliez pas de changer les permissions : 

```
# chown -R www:daemon /var/www/htdocs/drupal
```



Puisque nous en sommes à configurer la partie serveur //httpd//, éditez le fichier
``/etc/httpd.conf`` pour y ajouter votre nouveau site : 

```
server "chezmoi.tld" {
        listen on * port 80
        block return 301 "https://$SERVER_NAME$REQUEST_URI"
        no log
}

server "chezmoi.tld" {
        listen on * tls port 443
        root "/htdocs/drupal"
        directory index index.php
        connection max request body 537919488
        hsts
        tls {
            &SSLCERT
            &SSLKEY
        }

        location "*.php*" {
                fastcgi socket "/run/php-fpm.sock"
        }
}
```

À titre d'exemple, voici les manipulations à faire pour utiliser MariaDB (mysql)
avec drupal. Ces manipulations **ne sont pas à effectuer** si vous utilisez
SQLite.

Il faut créer un utilisateur pour la base de données qui aura le droit de
créer une nouvelle table. La table s'appellera ``drupal_base`` et l'utilisateur
``drupaluser``.
On entre ``# mysql -u root -p`` puis : 

```
MariaDB [(none)]> CREATE DATABASE drupal_base;
Query OK, 1 row affected (0.01 sec)

MariaDB [(none)]> CREATE USER 'drupaluser'@'localhost' IDENTIFIED BY 'motdepasse';
Query OK, 0 rows affected (0.01 sec)

MariaDB [(none)]> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX,
ALTER, CREATE TEMPORARY TABLES ON drupal_base.* TO 'drupaluser'@'localhost';
Query OK, 0 rows affected (0.02 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> exit
Bye
```


On a presque fini, courage.
Rechargez la configuration de //httpd// avec la commande habituelle 

```
# rcctl reload httpd
```

Vous pouvez maintenant terminer l'installation en ouvrant dans un navigateur
``https://chezmoi.tld/`` afin d'être redirigé vers la page d'installation.


[img/drupal1.png]

[img/drupal2.png]

[img/drupal3.png]

[img/drupal4.png]

[img/drupal5.png]

[img/drupal6.png]

[img/drupal7.png]

[img/drupal8.png]


Et voilà, vous êtes prêts à fabriquer votre site.
