Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4


==Synchronisation avec Syncthing==[syncthing]
[Syncthing https://syncthing.net/] permet de sauvegarder ses données et
facilement les répartir sur plusieurs appareils. Il dispose de plusieurs
//clients// (logiciels permettant de l'utiliser) pour Windows, Linux et même
**OpenBSD**. Par défaut, tout est chiffré ce qui est tout de même rassurant.


Notez qu'il a été empaqueté pour OpenBSD, on peut donc l'installer en une simple
commande : 

```
# pkg_add syncthing
```

Nous allons le laisser tourner en arrière-plan sur notre serveur afin que vous
puissiez à tout moment synchroniser vos documents et les sauvegarder.
Pour cela, lancez ces commandes : 

```
# rcctl enable syncthing
# rcctl start syncthing
```

Par défaut, syncthing va stocker sa configuration et les fichiers synchronisés
dans ``/var/syncthing``. Ce dossier contient :

- Un dossier ``Sync`` qui contient tous les documents sauvegardés.
- Un dossier ``.config`` qui contient la configuration.


Nous pourrions configurer syncthing en éditant ces fichiers, cependant, l'ajout
d'autres appareils avec lesquels se synchroniser va vite devenir insupportable.
Heureusement, il existe une interface d'administration pour syncthing. Elle
n'est disponible qu'à partir du serveur, ce qui n'est pas pratique pour s'en
servir car on a besoin d'un navigateur web pour y accéder. Heureusement, SSH est
là.

En effet, nous allons créer un tunnel SSH qui va relier votre ordinateur au
serveur. En passant par ce tunnel, nous pourrons accéder à l'interface
d'administration de syncthing très facilement.

À partir de votre ordinateur, lancez la commande suivante : 

```
ssh -N -L 9999:127.0.0.1:8384 -p 22222 utilisateurssh@&NDD
```

Remplacez 22222 par le port configuré dans la partie dédiée à [ssh #ssh], tout
comme l'utilisateur.

Tant que la session est ouverte, vous pouvez ouvrir un navigateur sur votre
ordinateur et aller à l'adresse ``http://localhost:9999`` afin d'administrer
syncthing : 

[img/syncthing.png]

Vous pouvez maintenant ajouter des machines avec lesquelles le serveur restera
synchronisé, comme si vous étiez **sur** le serveur.

Afin que tout fonctionne bien, vous devriez ouvrir les ports suivant dans le
pare-feu : 

- Port 22000 en TCP ;
- Port 21027 en UDP.

Je vous laisse visiter [le site officiel https://syncthing.net/] pour télécharger le client OpenBSD et en
savoir plus si vous le souhaitez. 


===Utilisation de syncthing===

Partager des documents avec Syncthing est relativement simple.
Pour commencer, sur l'ordinateur qui a les documents "sources", cliquez sur
"Ajouter un partage" : 

[img/syncthingshare1.png]


Choisissez ensuite un nom au partage afin de vous repérer. Laissez l'ID proposé
puis précisez le chemin racine du partage. Ce chemin correspond à l'emplacement
des données à sauvegarder, par exemple ``/home/jean-eudes/Documents``.
Si vous aviez déjà ajouté un appareil avec lequel partager, vous pouvez le
cocher en bas : 


[img/syncthingshare2.png]


Sinon, ajoutez l'appareil avec lequel partager vos documents. Pour cela, dans la
partie "Autres appareil", cliquez sur "Ajouter un appareil" : 


[img/syncthingshare3.png]

[img/syncthingshare4.png]


Il faut juste coller l'ID de l'appareil que vous pouvez trouver à l'emplacement
indiqué.

L'appareil ajouté recevra une notification à accepter, et le partage peut alors
commencer.
