Titre
Xavier Cartron
Dernière mise à jour: %%mtime(%d/%m/%Y)

%! encoding: utf-8
%!options: --toc --toc-level 4

===Un espace de stockage avec BoZoN===
[BoZoN http://bozon.pw/fr/] est ce qu'on peut appeler un cloud simplifié. Il vous
permettra de stocker vos documents sur votre serveur et de les partager
facilement. Il n'a pas besoin de base de données pour fonctionner et remplacera
dropbox dans la plupart des cas.

Tout ce dont vous aurez besoin pour Bozon, c'est du serveur [httpd #httpd] et de
[PHP #php].

On télécharge Bozon avec l'archive présente sur 
[github https://github.com/broncowdd/BoZoN/archive/master.zip] . Avec l'outil
``ftp``, on le fait ainsi : 

```
# ftp -o /tmp/bozon.zip "https://github.com/broncowdd/BoZoN/archive/master.zip"
```

On décompresse l'archive dans ``/var/www/htdocs``, puis on renomme le dossier
créé avant d'en changer les droits pour le serveur web (http) : 

```
# cd /var/www/htdocs
# unzip /tmp/bozon.zip
# mv BoZoN-master bozon
# chown -R www:daemon /var/www/htdocs/bozon
```

On peut maintenant éditer le fichier ``/etc/httpd.conf`` afin d'ajouter une
section pour BoZoN : 

```
server "bozon.&NDD" {
        listen on * port 80
        block return 301 "https://$SERVER_NAME$REQUEST_URI"
}

server "bozon.&NDD" {
        listen on * tls port 443
    	root "/htdocs/bozon" 
        directory index index.php
        hsts
        tls {
            &SSLCERT
            &SSLKEY
        }
        # Set max upload size to 513M (in bytes)
        connection max request body 537919488

        location "/*.php*" {
                fastcgi socket "/run/php-fpm.sock"
        }

        # Protect some files
        location "/uploads*"           { block }
        location "/private*"           { block }
        location "/thumbs*"            { block }
        location "/core/"              { block }
        location "/core/*.js"          { pass }
        location "/private/temp/*.zip" { pass }
}
```


On recharge //httpd// avec ``rcctl reload httpd`` puis on se dirige à l'adresse
``https://bozon.&NDD/`` pour finir l'installation.

Vous pouvez alors utiliser BoZon. Cliquez sur "Se connecter" pour créer un
compte administrateur.

Pour augmenter la restriction en taille des fichiers que vous aurez à
envoyer, vous devez modifier le fichier ``/etc/php-7.0.ini`` afin de changer les
valeurs dans les variables suivantes : 

```
post_max_size = 500M
upload_max_filesize = 500M
```
