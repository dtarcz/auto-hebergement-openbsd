

===  Statistiques des visites sur vos sites===
====Avec Webalizer====[webalizer]
[Webalizer http://www.patrickfrei.ch/webalizer/] est un outil qui peut générer
graphiques et tableaux à partir des logs (journaux) de votre serveur. En un
clin d'oeil vous pourrez trouver à propos de votre site : 

- Horaires de visites ;
- Nombre de clics ;
- Quantité de données chargées ;
- Pages 404 (pratique pour traquer les liens morts) ;
- D'où viennent les visiteurs...


D'autres outils similaires existent, en particulier [piwik https://piwik.org/].
Ce dernier est toutefois moins facile à mettre en place (base de données MySQL)
et nécessite l'insertion de code html dans toutes vos pages web. Il est 
aussi moins efficace si les visiteurs utilisent des addons Firefox comme noscript
ou ublock. Cependant, les données récoltées sont plus pertinentes. Vous voudrez
donc peut-être compléter l'installation de webalizer avec [piwik #piwik].

Quoi qu'il en soit, nous allons voir ici une méthode pour obtenir de belles
statistiques avec webalizer.

Comme d'habitude, on commence par l'installer avec la commande magique : 

```
# pkg_add webalizer
```

Pour le configurer, nous allons utiliser comme base le modèle fourni. On le
copie par exemple dans ``/etc/`` :  
```
# cp /usr/local/share/examples/webalizer/sample.conf /etc/webalizer.&NDD.conf
```

Éditez ce nouveau fichier pour l'adapter à vos besoins. Voici quelques options
pratiques que vous voudrez certainement changer : 

- ``LogFile        /var/www/logs/access.log`` : Emplacement des journaux du serveur
  web.
- ``OutputDir /var/www/htdocs/&NDD/stats`` : Vous choisissez où
  seront enregistrées les pages html générées. Référez-vous à la partie sur
  [httpd #httpd] pour configurer votre serveur web (http) et accéder aux statistiques
  avec un navigateur. Attention de bien créer ce répertoire au préalable, sinon Webalizer plantera avec un message d'erreur.
- ``HideSite *&NDD`` et ``HideReferrer &NDD/`` : On
  cache les liens provenant des clics réalisés sur votre site vers votre site.
- ``HideURL *.css , HideURL *.woff`` : On cache les extensions de fichiers non souhaitées.
- ``IgnoreURL /favicon.ico`` : On ignore certaines URL lorsque les statistiques
  sont générées.
- ``Color*`` : Pour changer les couleurs, car le thème par défaut, n'est pas
  très attrayant tout de même.
- ``HTMLHead ...`` : tout ce qui sera rajouté dans l'entête de la page html
  générée. Vous pouvez de cette façon ajouter quelques lignes CSS pour modifier
  l'apparence, par exemple : 

```
HTMLHead <style type="text/css">
HTMLHead body {
HTMLHead background: radial-gradient(circle,#2569A0,#000046);
HTMLHead }
HTMLHead </style>
```


Vous pouvez générer une première fois les statistiques avec la commande suivante :

```
# webalizer -c /etc/webalizer.&NDD.conf
```


Et hop, toutes les pages html et les graphiques sont dans le dossier défini par
la variable ``OutputDir``, il suffit de vous y rendre avec un navigateur web
pour les étudier.

Cependant, vous devrez régler encore quelques petits détails. Par
exemple, la partie des "Referers" qui recense les sites sur lesquels le vôtre
est cité doit être bien maigre. Il faut régler la façon dont //httpd// produit
les logs. Rien de bien compliqué, il faut juste ajouter dans le fichier
``/etc/httpd.conf`` la ligne suivante dans le site pour lequel on veut des
statistiques : ``log style combined``.

	Euh, on peut avoir un exemple siouplé?


Voici : 

```
server "&NDD" {
    listen on * tls port 443
    root "/htdocs/&NDD"
    directory index index.html
    log style combined

    hsts
    tls {
        &SSLCERT
        &SSLKEY
    }
}
```

N'oubliez pas de recharger //httpd// avec ``rcctl reload httpd``.


	Mais on doit lancer la commande webalizer manuellement ? C'est nul ce truc !


On n'en reste pas là bien entendu. Afin que les statistiques soient générées
par exemple tous les jours, nous pourrions profiter du fichier
``/etc/daily.local``. 

De plus, il faut éviter que les logs n'aient été archivés
avant d'avoir été traités par //webalizer//. 
Nous allons donc modifier la configuration de l'outil qui compresse les logs
régulièrement. Il s'agit de ``newsyslog``. On édite le fichier
``/etc/newsyslog.conf`` qui ressemble à ça : 

```
# $OpenBSD: newsyslog.conf,v 1.34 2015/10/14 20:54:07 millert Exp $
#
# configuration file for newsyslog
#
# logfile_name      owner:group     mode count size when  flags
/var/cron/log       root:wheel      600  3     10   *     Z
/var/log/aculog     uucp:dialer     660  7     *    24    Z
/var/log/authlog    root:wheel      640  7     *    168   Z
/var/log/daemon                     640  5     30   *     Z
/var/log/lpd-errs                   640  7     10   *     Z
/var/log/maillog                    640  7     *    24    Z
/var/log/messages                   644  5     30   *     Z
/var/log/secure                     600  7     *    168   Z
/var/log/wtmp                       644  7     *    $W6D4 B
/var/log/xferlog                    640  7     250  *     Z
/var/log/pflog                      600  3     250  *     ZB "pkill -HUP -u root -U root -t - -x pflogd"
/var/www/logs/access.log            644  4     *    $W0   Z "pkill -USR1 -u root -U root -x httpd"
/var/www/logs/error.log             644  7     250  *     Z "pkill -USR1 -u root -U root -x httpd"

```

C'est l'avant-dernière ligne que nous allons changer afin de lancer webalizer
avant de faire tourner les logs du serveur web (http). Elle ressemblera à  : 

```
/var/www/logs/access.log            644  4     *    $W0   Z "/usr/local/bin/webalizer -c /etc/webalizer.&NDD.conf && pkill -USR1 -u root -U root -x httpd"
```

Pour vérifier que tout fonctionne bien, lancez newsyslog en le forçant à
archiver les logs et en le faisant parler. Vous devriez obtenir quelque chose de
la sorte : 


```
# newsyslog -vF
/var/cron/log <3Z>: size (KB): 7.24 [10] --> trimming log....
/var/log/authlog <7Z>: age (hr): 88 [168] --> trimming log....
/var/log/daemon <5Z>: size (KB): 0.41 [30] --> trimming log....
/var/log/lpd-errs <7Z>: size (KB): 2.02 [10] --> trimming log....
/var/log/maillog <7Z>: age (hr): 16 [24] --> trimming log....
/var/log/messages <5Z>: size (KB): 0.45 [30] --> trimming log....
/var/log/secure <7Z>: age (hr): -1 [168] --> trimming log....
/var/log/wtmp <7B>: --> trimming log....
/var/log/xferlog <7Z>: size (KB): 0.00 [250] --> trimming log....
/var/log/pflog <3ZB>: size (KB): 64.26 [250] --> trimming log....
/var/www/logs/access.log <4Z>: --> trimming log....
/var/www/logs/error.log <7Z>: size (KB): 212.87 [250] --> trimming log....
/var/www/logs/mateteestmalade.log <7Z>: size (KB): 3.93 [250] --> trimming log....
Webalizer Xtended RB30 (06-Apr-2014) / OpenBSD 5.9 amd64 / English
Copyright 2005-2014 by Patrick K. Frei
Based on Webalizer V2.23-08
Using logfile /var/www/logs/access.log (clf)
Using GeoIP Country Edition (/var/db/GeoIP/GeoIP.dat)
GEO-106FREE 20151201 Build 1 Copyright (c) 2015 MaxMind Inc All Rights Reserved
Creating output in /var/www/htdocs/&NDD/stats
Hostname for reports is '&NDD'
Reading history file... webalizer.hist
Skipping bad record (1)
No valid records found!
Generating summary report

```

Il y a les messages de webalizer qui montrent qu'il a été exécuté.

Et voilà, les statistiques sont générées régulièrement et avant que les logs ne soient
archivés.


====Avec Piwik====[piwik]
[Piwik https://piwik.org] est nettement plus lourd pour générer des statistiques. Si votre serveur a une
puissance limitée, préférez [webalizer #webalizer].


Je ne vais pas détailler l'installation pas à pas dans cette partie. Je suppose
donc que vous avez lu et compris la partie sur [PHP #php] ainsi que celle sur
[MySQL #mysql].

Pour PHP, installez et activez ces paquets : ``php-curl php-gd``. Il y a
aussi besoin des bibliothèques geoip et cli qui sont normalement intégrées dans
le paquet PHP d'OpenBSD.

Installez une base [MySQL (mariadb) #mysql]. Voici un récapitulatif tiré de la
[documentation de piwik https://piwik.org/docs/requirements/]: 

```
# mysql -u root -p
mysql> CREATE DATABASE piwik_db_name_here;
mysql> CREATE USER 'piwik'@'localhost' IDENTIFIED BY 'password';
mysql> GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON piwik_db_name_here.* TO 'piwik'@'localhost';

```

Ensuite, on télécharge piwik dans le dossier ``/var/www/htdocs/piwik`` : 

```
# mkdir -p /var/www/htdocs/piwik
# cd /var/www/htdocs/piwik
# ftp -o /tmp/piwik.zip "http://builds.piwik.org/piwik.zip" && unzip /tmp/piwik.zip
```

Configurez maintenant un nouveau site dans le fichier ``/etc/httpd.conf`` : 

```
server "stats.&NDD" { 
    listen on * tls port 443 
    root "/htdocs/piwik/piwik/"
    directory index index.php

    hsts 
    tls {
        &SSLCERT
        &SSLKEY
    }

    location "*.php*" {
        fastcgi socket "/run/php-fpm.sock"
    }
}
```

Modifiez les droits sur ce dossier :

```
# chown -R www:daemon /var/www/htdocs/piwik
```


Rechargez //httpd// avec ``rcctl reload httpd`` puis dirigez-vous avec un navigateur
sur l'adresse du site fraîchement activé pour terminer l'installation de piwik.

N'oubliez pas d'ajouter à vos pages web le code d'intégration donné.
